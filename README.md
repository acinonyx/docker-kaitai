# SatNOGS Kaitai Struct Docker image #

This is the repository for SatNOGS Kaitai Struct Docker image.

GitLab: [docker-kaitai](https://gitlab.com/librespacefoundation/satnogs/docker-kaitai)
Docker Hub: [librespace/kaitai](https://hub.docker.com/r/librespace/kaitai/)
